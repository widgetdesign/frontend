import React, { useState, useEffect } from "react";
import { useHistory, useLocation } from "react-router-dom";
import "./payment.css";
import { getPaymentData } from "../services";
import { showError } from "../utils";

export default function Payment() {
  const [selectedAmount, setSelectedAmount] = useState(null);
  const [selectedCategory, setSelectedCategory] = useState(null);
  const [donationType, setDonationType] = useState(null);
  const [paymentDetails, setPaymentDetails] = useState([]);
  const [progressPercent, setProgressPercent] = useState({
    1: 0,
    2: 0,
    3: 0,
  });
  useEffect(() => {
    getPaymentDetails();
  }, []);
  useEffect(() => {
    progressBarData();
  }, [paymentDetails]);

  const handleSelectedAmount = (value) => {
    setSelectedAmount(value);
  };
  const getPaymentDetails = async () => {
    const response = await getPaymentData();
    const mappedData =
      response &&
      response.data &&
      response.data.reduce(
        (obj, item) => Object.assign(obj, { [item._id]: item.sum }),
        {}
      );
    setPaymentDetails(mappedData);
  };
  const progressBarData = () => {
    const atRiskYouth = paymentDetails["1"] ? paymentDetails["1"] : 0;
    const domesticViolence = paymentDetails["2"] ? paymentDetails["2"] : 0;
    const mentalHealthChallenges = paymentDetails["3"]
      ? paymentDetails["3"]
      : 0;
    const totalPayment =
      atRiskYouth + domesticViolence + mentalHealthChallenges;
    const total = totalPayment / 10;
    const p1 = atRiskYouth / total;
    const p2 = domesticViolence / total;
    const p3 = mentalHealthChallenges / total;
    setProgressPercent({
      1: p1 * 100,
      2: p2 * 100,
      3: p3 * 100,
    });
  };

  const { push } = useHistory();
  const handlePay = () => {
    if (selectedCategory !== null) {
      push(
        `/pay/?payAmt=${selectedAmount}&category=${selectedCategory}&type=${donationType}`
      );
    } else {
      showError("Select payment category");
    }
  };
  return (
    <>
      <div className="">
        <div className="row">
          <div className="col-sm-2">
            <div>
              <img src="images/logo.jpg" alt="" className="logo" />
            </div>
          </div>
          <div className="col-sm-10 NGOtext">
            <h1>The Yoga Foundation</h1>
            <h4>Share the Gift of Yoga</h4>
          </div>
        </div>
      </div>
      <div className="mainimg">
        <div className="container">
          <div className="row">
            <div className="col-sm-12"></div>
          </div>
        </div>
      </div>
      <div className="payment">
        <div className="">
          <div className="row">
            <div className="col-sm-12" style={{ marginTop: "15px" }}>
              <div className="donationbtn">
                <button
                  type="button"
                  className="btn d-flex offdon"
                  onClick={() => {
                    setDonationType("One-Off Donation");
                  }}
                >
                  One-Off Donation
                </button>
                <button
                  type="button"
                  className="btn d-flex"
                  style={{ marginLeft: "10px" }}
                  onClick={() => {
                    setDonationType("Regular Donation");
                  }}
                >
                  Regular Donation
                </button>
              </div>
            </div>
          </div>
          <div className="row">
            <div
              className="col-sm-12"
              style={{ marginTop: "15px", marginBottom: "15px" }}
            >
              <div className="amtbtn">
                <button
                  className="btn d-flex"
                  type="button"
                  onClick={() => handleSelectedAmount(15)}
                >
                  $15
                </button>
                <button
                  className="btn d-flex"
                  type="button"
                  onClick={() => handleSelectedAmount(50)}
                >
                  $50
                </button>
                <button
                  className="btn d-flex"
                  type="button"
                  onClick={() => handleSelectedAmount(100)}
                >
                  $100
                </button>
                <button
                  className="btn d-flex"
                  type="button"
                  onClick={() => handleSelectedAmount(150)}
                >
                  $150
                </button>
                <button
                  className="btn d-flex"
                  type="button"
                  onClick={() => handleSelectedAmount(500)}
                >
                  $500
                </button>
                <input
                  type="number"
                  value={selectedAmount ? selectedAmount : ""}
                  onChange={(e) => handleSelectedAmount(e.target.value)}
                />
                <a>
                  <button
                    className="btn-danger d-flex paynow"
                    onClick={handlePay}
                  >
                    Give Now
                  </button>
                </a>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="radio">
              <div
                className="col-md-3"
                style={{ marginTop: "15px", marginBottom: "15px" }}
              >
                <label>
                  <input
                    type="radio"
                    name="1"
                    onClick={() => setSelectedCategory(1)}
                  />
                  <div className="dollars">At-risk youth</div>
                </label>
              </div>
              <div
                className="col-md-4"
                style={{ marginTop: "15px", marginBottom: "15px" }}
              >
                <label>
                  <input
                    type="radio"
                    name="2"
                    onClick={() => setSelectedCategory(2)}
                  />
                  <div className="dollars">Domestic violence</div>
                </label>
              </div>
              <div
                className="col-md-5"
                style={{ marginTop: "15px", marginBottom: "15px" }}
              >
                <label>
                  <input
                    type="radio"
                    name="3"
                    onClick={() => setSelectedCategory(3)}
                  />
                  <div className="dollars">Mental health challenges</div>
                </label>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-sm-8" style={{ padding: "20px 40px" }}>
          <div className="">
            <h2>Progress</h2>
            <div className="panel panel-default">
              <div className="panel-body">
                <div className="titletext">At-risk youth</div>
                <div className="dollars">
                  $
                  {paymentDetails && paymentDetails["1"]
                    ? paymentDetails["1"]
                    : 0}
                </div>
                <div className="progress">
                  <div
                    className="progress-bar"
                    role="progressbar"
                    style={{ width: progressPercent["1"] }}
                  >
                    <span className="circle"></span>
                  </div>
                </div>
              </div>
              <div className="panel-body">
                <div className="titletext">Domestic violence</div>
                <div className="dollars">
                  $
                  {paymentDetails && paymentDetails["2"]
                    ? paymentDetails["2"]
                    : 0}
                </div>
                <div className="progress">
                  <div
                    className="progress-bar"
                    role="progressbar"
                    style={{ width: progressPercent["2"] }}
                  >
                    <span className="circle"></span>
                  </div>
                </div>
              </div>
              <div className="panel-body">
                <div className="titletext">Mental health challenges</div>
                {/* <i> */}
                <div className="dollars">
                  $
                  {paymentDetails && paymentDetails["3"]
                    ? paymentDetails["3"]
                    : 0}
                </div>
                <div className="progress">
                  <div
                    className="progress-bar"
                    role="progressbar"
                    // style="width: 75%"
                    style={{ width: progressPercent["3"] }}
                  >
                    <span className="circle"></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
  // return <>hererher</>;
}

import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "./Home";
import Payment from "./Payment";
import Pay from "./Pay";
import Thankyou from "./Thankyou";

export default function MainRoutes() {
  return (
    <Router>
      <Route exact path="/" component={Home} />
      <Route path="/payment" component={Payment} />
      <Route exact path="/pay" component={Pay} />
      <Route path="/thankyou" component={Thankyou} />
    </Router>
  );
}

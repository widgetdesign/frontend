import react, { useEffect } from "react";
import "./App.css";
import MainRoutes from "./routes";

function App() {
  useEffect(() => {
    loadStripe();
  }, []);
  const loadStripe = () => {
    if (!window.document.getElementById("stripe-script")) {
      var s = window.document.createElement("script");
      s.id = "stripe-script";
      s.type = "text/javascript";
      s.src = "https://js.stripe.com/v2/";
      s.onload = () => {
        window["Stripe"].setPublishableKey(
          "pk_test_51ILsHsCO1T5XCvpOXvpAk6JLhWwqFqaDwPWEDjrgcG7Xbx1BRvLKdNAwhG11Hq8EqGPwn8AoDfiHNMYzM8p3SC7N00f7yMbthw"
        );
      };
      window.document.body.appendChild(s);
    }
  };
  return <MainRoutes />;
}

export default App;

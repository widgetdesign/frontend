import React from "react";

export default function Thankyou() {
  return (
    <div>
      <div className="container">
        <div className="row">
          <div className="col-sm-2">
            <div>
              <img src="images/logo.jpg" alt="" className="logo" />
            </div>
          </div>
          <div className="col-sm-10 NGOtext">
            <h1>The Yoga Foundation</h1>
            <h4>Share the Gift of Yoga</h4>
          </div>
        </div>
      </div>

      <div className="container">
        <div className="row">
          <div className="col-sm-12">
            <div className="thanksimg">
              <div className="thankyou"></div>
              {/* <img src="images/image0.jpg" alt="" /> */}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

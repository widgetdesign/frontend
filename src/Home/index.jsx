import React from "react";
import { useHistory } from "react-router-dom";

export default function Home() {
  const { push } = useHistory();
  const handlePayClick = () => {
    push("/payment");
  };
  return (
    <div>
      <div
        style={{
          display: "flex",
          height: "100vh",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <button
          className="btn"
          style={{ width: "8%" }}
          onClick={handlePayClick}
        >
          Donate
        </button>
      </div>
    </div>
  );
}

import axios from "axios";

export const postPaymentData = (data) => {
  return axios.request({
    headers: { "Content-Type": "application/json" },
    url: "http://localhost:5000/api/savepayment",
    method: "POST",
    data: data,
  });
};

export const getPaymentData = () => {
  return axios.request({
    headers: { "Content-Type": "application/json" },
    url: "http://localhost:5000/api/getDetails",
    method: "GET",
  });
};

export const finalizePayment = ({ token, amount }) => {
  return axios.request({
    headers: { "Content-Type": "application/json" },
    url: "http://localhost:5000/api/stripePayment",
    method: "POST",
    data: { stripeToken: token, amount: amount },
  });
};

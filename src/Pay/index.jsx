import React, { useState } from "react";
import { Field, Form } from "react-final-form";
import { useHistory, useLocation } from "react-router-dom";
import queryString from "query-string";
import { postPaymentData, finalizePayment } from "../services";
import { showSuccess, showError } from "../utils";
import Loader from "../Loader";
import "./pay.css";
import stripe from "stripe";

// const stripe = require("stripe")(
//   "sk_test_51ILsHsCO1T5XCvpOdPGnxMUbRZxF3KyiLMwa8cZg1nMoaOEhrUqJVyDvCTIg05PjSetOMjzNC9RZcb6O8ejrvdHo00yAQNpwfn"
// );
export default function Pay() {
  const { push } = useHistory();
  const location = useLocation();
  const queries = queryString.parse(location.search);
  const [isLoading, setIsLoading] = useState(false);
  const StripeAPI = stripe(
    "sk_test_51ILsHsCO1T5XCvpOdPGnxMUbRZxF3KyiLMwa8cZg1nMoaOEhrUqJVyDvCTIg05PjSetOMjzNC9RZcb6O8ejrvdHo00yAQNpwfn"
  );
  const handleCreditCardValidation = (value) => {
    if (value && value.length !== 16) {
      return "*Card must be 16digit";
    } else {
      return undefined;
    }
  };

  const stripePaymentGetToken = async (value) => {
    window.Stripe.card.createToken(
      {
        number: value.cardnumber,
        exp_month: value["exp-month"],
        exp_year: value.expyear,
        cvc: value.cvv,
      },
      async (status, response) => {
        if (status === 200) {
          // setGeneratedToken(response.card.id);
          try {
            setIsLoading(true);
            const res = await finalizePayment({
              token: response.id,
              amount: value.amount,
            });
            if (res) {
              showSuccess("Success Payment");

              const response = await postPaymentData({
                ...value,
                category: queries.category,
                Donationtype: queries.type,
              });

              if (response.data) {
                push("/thankyou");
              } else {
                showError("Failed Payment");
              }
            }
          } catch (e) {
            showError("Failed Payment");
          }
        } else {
          showError("Failed Payment");
        }
      }
    );
  };

  const handleAddPayment = async (values) => {
    await stripePaymentGetToken(values);
  };
  return (
    <>
      <div className="container-fluid">
        <div className="row">
          <div className="col-sm-2">
            <div>
              <img src="images/logo.jpg" alt="" className="logo" />
            </div>
          </div>
          <div className="col-sm-7 NGOtext">
            <h1>The Yoga Foundation</h1>
            <h4>Share the Gift of Yoga</h4>
          </div>
          <div className="col-sm-3">
            <a>
              <button
                className="btn btn-primary"
                style={{ width: "100px" }}
                onClick={() => push("/payment")}
              >
                Back
              </button>
            </a>
          </div>
        </div>
      </div>

      <div className="container-fluid">
        {isLoading && <Loader />}

        <div className="row">
          <Form
            onSubmit={handleAddPayment}
            initialValues={{
              amount: queries ? queries.payAmt : "",
            }}
            render={({ handleSubmit, form, values, errors }) => {
              return (
                <>
                  <div className="col-75">
                    <form>
                      <div className="row">
                        <div className="col-50">
                          <h3>Billing Address</h3>
                          <div>
                            <label>Name</label>
                            <Field
                              name="name"
                              component="input"
                              type="text"
                              placeholder="Name"
                            />
                          </div>
                          <div>
                            <label>Email</label>
                            <Field
                              name="email"
                              component="input"
                              type="text"
                              placeholder="Email"
                            />
                          </div>
                          <div>
                            <label>Address</label>
                            <Field
                              name="address"
                              component="input"
                              type="text"
                              placeholder="Address"
                            />
                          </div>
                          <div>
                            <label>City</label>
                            <Field
                              name="city"
                              component="input"
                              type="text"
                              placeholder="City"
                            />
                          </div>
                          <div className="row">
                            <div className="col-50">
                              <div>
                                <label>State</label>
                                <Field
                                  name="state"
                                  component="input"
                                  type="text"
                                  placeholder="State"
                                />
                              </div>
                            </div>
                            <div className="col-50">
                              <div>
                                <label>Zip</label>
                                <Field
                                  name="zip"
                                  component="input"
                                  type="text"
                                  placeholder="Zip"
                                />
                              </div>
                            </div>
                          </div>
                        </div>

                        <div className="col-50">
                          <h3>Payment</h3>
                          <div>
                            <label>Amount</label>
                            <Field
                              name="amount"
                              component="input"
                              type="text"
                              placeholder="Amount"
                            />
                          </div>
                          <div>
                            <label>Name on card</label>
                            <Field
                              name="nameoncard"
                              component="input"
                              type="text"
                              placeholder="As per card"
                            />
                          </div>
                          <div>
                            <Field
                              name="cardnumber"
                              validate={handleCreditCardValidation}
                            >
                              {({ input, meta }) => (
                                <div>
                                  <label>
                                    Credit card number
                                    {meta.error && meta.touched && (
                                      <span className="text-danger ml-40">
                                        {meta.error}
                                      </span>
                                    )}
                                  </label>
                                  <input
                                    {...input}
                                    type="text"
                                    placeholder="16-digit number"
                                  />
                                </div>
                              )}
                            </Field>
                          </div>
                          <div>
                            <label>Expire Month</label>
                            <Field
                              name="exp-month"
                              component="input"
                              type="text"
                              placeholder="Month"
                            />
                          </div>
                          <div className="row">
                            <div>
                              <label>Exp year</label>
                              <Field
                                name="expyear"
                                component="input"
                                type="text"
                                placeholder="Exp Year"
                              />
                            </div>
                            <div className="col-50">
                              <div>
                                <label>CVV</label>
                                <Field
                                  name="cvv"
                                  component="input"
                                  type="text"
                                  placeholder="CVV"
                                />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <a>
                        <button
                          className="btn"
                          type="submit"
                          onClick={handleSubmit}
                        >
                          Add Payment
                        </button>
                      </a>
                    </form>
                  </div>
                </>
              );
            }}
          />
        </div>
      </div>
    </>
  );
}

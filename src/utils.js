import toastr from "toastr";

export const showSuccess = (message) => {
  toastr.success(message);
};
export const showError = (message) => {
  toastr.error(message);
};
